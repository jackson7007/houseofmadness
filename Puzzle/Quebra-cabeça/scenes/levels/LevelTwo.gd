extends Node2D


var piece0 = false
var piece1 = false
var piece2 = false
var piece3 = false
var piece4 = false
var piece5 = false
var piece6 = false
var piece7 = false
var piece8 = false


func _ready():
	$Button.hide()

func _process(delta):
	if $Piec0.global_position == $Pos0.global_position:
		piece0 = true
		$Piec0/AreaPieces.input_pickable=false
		
	if $Piec1.global_position == $Pos1.global_position:
		piece1 = true
		$Piec1/AreaPieces.input_pickable=false
		
	if $Piec2.global_position == $Pos2.global_position:
		piece2 = true
		$Piec2/AreaPieces.input_pickable=false
		
	if $Piec3.global_position == $Pos3.global_position:
		piece3 = true
		$Piec3/AreaPieces.input_pickable=false
		
	if $Piec4.global_position == $Pos4.global_position:
		piece4 = true
		$Piec4/AreaPieces.input_pickable=false
		
	if $Piec5.global_position == $Pos5.global_position:
		piece5 = true
		$Piec5/AreaPieces.input_pickable=false
		
	if $Piec6.global_position == $Pos6.global_position:
		piece6 = true
		$Piec6/AreaPieces.input_pickable=false

	if $Piec7.global_position == $Pos7.global_position:
		piece7 = true
		$Piec7/AreaPieces.input_pickable=false

	if $Piec8.global_position == $Pos8.global_position:
		piece8 = true
		$Piec8/AreaPieces.input_pickable=false

	if piece0 == true and piece1 == true and piece2 == true and piece3 == true and piece4 == true and piece5 == true and piece6 == true and piece7 == true and piece8 == true:
		_win()
	pass

func _win():
	$Button.show()

func _on_Button_pressed():
	queue_free()
