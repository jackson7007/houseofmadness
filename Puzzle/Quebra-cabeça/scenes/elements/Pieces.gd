extends AnimatedSprite

var inside     = false
var placement  = false
var areaCount  = 0
var clickMouse = Vector2()
var memory     = Vector2()

func _process(delta):
	if inside == true:
		self.set_global_position(get_global_mouse_position() + clickMouse)
	if placement == true and Input.is_action_just_released("left_click"):
		self.set_position(memory)
		placement = false


func _on_AreaPieces_area_entered(area):
	areaCount += 1 
	memory = area.global_position
	placement = true 
 

func _on_AreaPieces_area_exited(area):
	areaCount -= 1 
	if areaCount == 0:
		placement = false

func _on_AreaPieces_input_event(viewport, event, shape_idx): 
	if event.is_action_pressed("left_click"):
		inside = true
		self.z_index = 1
		clickMouse = self.get_global_position() - get_global_mouse_position()
	if event.is_action_released("left_click"):
		inside = false
		self.z_index = 0
