extends VBoxContainer

onready var level_label: Label = $Level
onready var fps_label: Label = $FPS

func _ready():
	if GameStateMachinecaminho.state == GameStateMachinecaminho.states.GAME:
		$ResetButton.visible = true

func _process(_delta):
	level_label.text = "Level " + GameStateMachinecaminho.level_progress as String
	fps_label.text = Engine.get_frames_per_second() as String + " fps"

func _on_ResetButton_pressed():
	GameStateMachinecaminho.reset_level()
