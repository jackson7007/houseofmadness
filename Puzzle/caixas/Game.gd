extends Node2D
#ARRUMAR AQUI <P<S 

onready var spots_node     = get_node("Spots")
#onready var current_level = get_node("Level").get_child(0)
#scene changer ? current lvl?
var game_end = false

#var moves = 0

func _process(_delta): 
	spots_node = get_node("Spots")
	#current_level = get_node("Level").get_child(0)
	var spot_count = spots_node.get_child_count()
	
	if !game_end:
		for i in spots_node.get_children():
			if i.active == true:
				spot_count -= 1
	if spot_count == 0:
		$UI/AcceptDialog.popup()
		game_end  = true

		# Por para ir pro prox lvl qnd criar

func check_end():
	if game_end == false:
		var spots = $Spots.get_child_count()
		for i in $Spots.get_children():
			if i.occupied:
				spots -= 1
		
		if spots == 0:
			$UI/AcceptDialog.popup()
			game_end = true

func _on_AcceptDialog_confirmed():
	get_tree().reload_current_scene()

func _on_TextureButton_pressed():
	get_tree().reload_current_scene()
