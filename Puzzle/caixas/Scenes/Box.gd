extends KinematicBody2D

onready var sprite    = $Sprite
onready var glow_mat  = preload("res://Puzzle/caixas/src/glow_sprite_material.tres")
onready var green_box = preload("res://Puzzle/caixas/src/box_green.png")
onready var ray       = $RayCast2D

var grid_size = 16
var glow      = false setget set_glow
var inputs    = {
	'ui_up': Vector2.UP,
	'ui_down': Vector2.DOWN,
	'ui_left': Vector2.LEFT,
	'ui_right': Vector2.RIGHT
}

func move(dir):
	var vector_pos = inputs[dir] * grid_size
	ray.cast_to = vector_pos
	ray.force_raycast_update()
	$Tween.interpolate_property(
		self, "position",
		position, position + vector_pos, 0.1,
		Tween.TRANS_SINE,
		Tween.EASE_IN_OUT
	)
	if !ray.is_colliding():
		$Tween.start()
		return true
	return false

func set_glow(glow):
	if glow:
		sprite.set_material(glow_mat)
	else:
		sprite.set_material(null)
	
