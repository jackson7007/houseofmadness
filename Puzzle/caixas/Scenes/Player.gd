extends KinematicBody2D

onready var ray   = $RayCast2D
onready var anim  = $AnimationPlayer
onready var tween = $Tween

var speed     = 10
var tile_size = 16
var inputs    = {
	'ui_up': Vector2.UP,
	'ui_down': Vector2.DOWN,
	'ui_left': Vector2.LEFT,
	'ui_right': Vector2.RIGHT
}

func _ready():
	position = position.snapped(Vector2.ONE * tile_size)
	
func _unhandled_input(event):
	if tween.is_active():
		return
	for dir in inputs.keys():
		if event.is_action_pressed(dir):
				move(dir)

func move(dir):

	ray.cast_to = inputs[dir] * tile_size
	ray.force_raycast_update()
	if !ray.is_colliding():
	#	position += inputs[dir] * tile_size
		move_tween(dir)
	else:
		var collider = ray.get_collider()
		if collider.is_in_group("box"):
			if collider.move(dir):
				move_tween(dir)

func move_tween(dir):
	anim.play("walk")
	tween.interpolate_property(self, "position", position, 
		position + inputs[dir] * tile_size, 1.0/speed, Tween.TRANS_SINE, 
		Tween.EASE_IN_OUT)
	tween.start()

