extends Node2D

export (String) var level_code

export (bool) var enabled
export (Vector2) var select_screen_pos
var score_goal_met = false

var next_path_resource
var next_rat_resource

var next_paths = {}
var next_rats = {}

onready var level_label = $Desc/Label
onready var button = $TextureButton
onready var star = $Desc/Star

# Called when the node enters the scene tree for the first time.
func _ready():
	
	next_path_resource = load("res://Puzzle/rotas/Scenes/next_path.tscn")
	next_rat_resource = load("res://Puzzle/rotas/Scenes/next_rat.tscn")
	
	# If the level is unlocked
	if GameDataManagerrota.level_info.has(level_code):
		enabled = GameDataManagerrota.level_info[level_code].unlocked
		score_goal_met = GameDataManagerrota.level_info[level_code].score_goal_met
		
		# If the level is unlocked but not cleared:
		if !GameDataManagerrota.level_info[level_code].solved:
			$Highlight.visible = true
	else:
		enabled = false
	
	for next_level_code in Globalsrota.levels[level_code].next_level_codes:
		# If the next level exists
		if Globalsrota.levels.has(next_level_code):
			
			# Create the "Next" traces and make them go to the location of the next levels
			var next_level_pos = get_parent().get_node(next_level_code).position
			
			var next_path = next_path_resource.instance()
			next_paths[next_level_code] = next_path
			var next_rat = next_rat_resource.instance()
			next_rats[next_level_code] = next_rat
			
			# Position the points so that they start and end on the borders of
			next_path.points[0] = (next_level_pos - position).normalized() * 30
			next_path.points[1] = (next_level_pos - position) - (next_level_pos - position).normalized() * 30
			
			next_rat.points[1] = (next_level_pos - position)
			
			$PathsLayer.add_child(next_rat)
			$PathsLayer.add_child(next_path)
			
			
			# If the next level is unlocked
			if GameDataManagerrota.level_info.has(next_level_code):
				next_path.visible = true
				# If it's the first time the player sees the menu after the level has been unlocked
				if GameDataManagerrota.level_info[next_level_code].just_unlocked:
					
					$PosTween.interpolate_property(next_path, "points", next_path.points[0], next_path.points[1], 0.5, Tween.TRANS_QUAD)
					$WidthTween.interpolate_property(next_path, "width", 0, 18, 0.5, Tween.TRANS_QUAD)
					$PosTween.start()
					$WidthTween.start()
					
					GameDataManagerrota.level_info[next_level_code].just_unlocked = false
					GameDataManagerrota.save_data()
			# If the next level exists but isn't unlocked
			else:
				next_rat.visible = true
	
	# If it's the first time the player sees the menu after getting the star
	if GameDataManagerrota.level_info.has(level_code) and GameDataManagerrota.level_info[level_code].just_got_goal:
		$AnimationStar.play("star_anim")
		GameDataManagerrota.level_info[level_code].just_got_goal = false
		GameDataManagerrota.save_data()
			
	level_label.set_text(Globalsrota.level_code_to_text(level_code))
	
	_on_change_color()
	
	var options_panel = get_tree().get_root().find_node("OptionsPanel",true,false)
	options_panel.connect("change_color", self, "_on_change_color")
	
func _on_change_color():
	level_label.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].text2)
	
	if enabled:
		button.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].base[0])
#		previous_path.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].base[0])
	else:
		button.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].gray_disabled)
#		previous_path.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].ratline)
		
	if score_goal_met:
		star.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].star_filled)
	else:
		star.set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].star_blank)
	
	for next_level_code in Globalsrota.levels[level_code].next_level_codes:
		next_paths[next_level_code].set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].base[0])
		next_rats[next_level_code].set_modulate(Globalsrota.Colors[ConfigManagerrota.color_palette].ratline) 

func _on_TextureButton_pressed():
	if enabled:
		AudioManagerrota.play_button("confirm")
		var _scene = get_tree().change_scene("res://Puzzle/rotas/Scenes/levels/level_" + level_code + ".tscn")


func _on_PosTween_tween_step(object, _key, _elapsed, value):
	object.points[1] = value
