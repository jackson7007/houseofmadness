extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var options_panel = get_tree().get_root().find_node("OptionsPanel",true,false)
	options_panel.connect("change_color", self, "_on_change_color")
	
func _on_change_color():
	update()
	
func _draw():
	draw_rect(Rect2(0, 0, Globalsrota.DISPLAY_WIDTH, Globalsrota.DISPLAY_HEIGHT), Globalsrota.Colors[ConfigManagerrota.color_palette].background)
	for i in (1 + Globalsrota.DISPLAY_WIDTH/Globalsrota.GRID_SIZE):
		for j in (1 + Globalsrota.DISPLAY_HEIGHT/Globalsrota.GRID_SIZE):
			draw_circle(Vector2(Globalsrota.GRID_SIZE * i, Globalsrota.GRID_SIZE * j), 1, Globalsrota.Colors[ConfigManagerrota.color_palette].gray_dots)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
