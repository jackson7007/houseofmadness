extends Node

const Difficulty = preload("res://Puzzle/Luzes/assets/scripts/consts/difficulty.gd")
const GameType = preload("res://Puzzle/Luzes/assets/scripts/consts/game_type.gd")

var game_type = GameType.STANDARD
var difficulty = Difficulty.EASY
var guide_marker = false
