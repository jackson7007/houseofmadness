extends Node

func _ready():
	$GameTypeDropdown.select(GlobalSettingsluz.game_type)
	$DifficultyDropdown.select(GlobalSettingsluz.difficulty)
	
	$EasyModeHighScore.text = "Easy: " + str(Globalluz.easy_high_score)
	$NormalModeHighScore.text = "Normal: " + str(Globalluz.normal_high_score)
	$HardModeHighScore.text = "Hard: " + str(Globalluz.hard_high_score)
	
	$EasyModeHighScoreTimed.text = "Easy: " + str(Globalluz.easy_high_score_timed)
	$NormalModeHighScoreTimed.text = "Normal: " + str(Globalluz.normal_high_score_timed)
	$HardModeHighScoreTimed.text = "Hard: " + str(Globalluz.hard_high_score_timed)
