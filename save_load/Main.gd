extends Control
#https://www.youtube.com/watch?v=_gBpk5nKyXU

onready var _save_game_dlg =  $Node/SaveGameDlg
onready var _load_game_dlg =  $Node/LoadGameDlg
var animation_is_active = false

func _ready():
	pass
	
func activate_animation(state): 
	animation_is_active = state
	
func _on_save_pressed():
	#if $lama.play("lama 1") and not animation_is_active:
		$Node/SaveGameDlg.visible=true
		_save_game_dlg.show_modal(true)
	
func _on_load_pressed():
	##$lama.play("lama 2")
	$Node/LoadGameDlg.visible=true
	_load_game_dlg.show_modal(true)

func _on_back_pressed():
	self.visible = false
