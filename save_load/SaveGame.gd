extends Control

#C:\Users\USER\AppData\Roaming\House of madness

class_name SaveGameDlg
const SAVE_GAME_FOLDER = "user://saved_games"

onready var _texture_rect: TextureRect = $TextureRect
var _save_game_image: Image = Image.new()
#onready var lp = get_node("Label")

func _ready():
	pass

func _input (event):
	if event.is_action_pressed("Escape"):
		queue_free()

func show_save_dlg(save_img: Image) -> void:
	_save_game_image = save_img
	var texture = ImageTexture.new()
	texture.create_from_image(save_img)
	_texture_rect.texture = texture
	
	show_modal(true)
	
func _get_date_time_string():
	var datetime = OS.get_datetime()
	return "%d%02d%02d_%02d%02d%02d" % [datetime["day"], datetime["month"], datetime["year"], datetime["hour"], datetime["minute"], datetime["second"]]

func _on_ok_pressed():
	var dir = Directory.new()
	dir.make_dir_recursive(SAVE_GAME_FOLDER)
	var date_string = _get_date_time_string()
	var save_game_file_name = SAVE_GAME_FOLDER + "/" + date_string + ".tres"
	if GameStateService.save(save_game_file_name):
		var image_file_name = SAVE_GAME_FOLDER + "/" + date_string + ".png"
		print(_save_game_image)
		_save_game_image.save_png(image_file_name)
	emit_signal("popup_hide")
	
func _on_cancel_pressed():
	self.visible=false


