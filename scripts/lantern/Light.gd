extends Node2D

onready var lamp           = load("res://scenes/lantern/Light.tscn")
onready var on_hand_sprite = $lantern
onready var player         = $".."

var is_visible             = false 

func _ready():
	$lantern/light.hide()

func _unhandled_input(event):
	if event.is_action_pressed("equip_light"):
		#$lantern/LigarLamp.play()
		is_visible=!is_visible
		if is_visible: 
			$lantern/light.show()
			player.walk_light = true
		else:
			$lantern/light.hide()
			player.walk_light = false
			
	
func set_visible(is_visible):
	for node in get_children():
		node.visible=is_visible
