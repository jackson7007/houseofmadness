extends KinematicBody2D

export var WANDER_TARGET_RANGE = 6
export var ACCELERATION        = 400
export var MAX_SPEED           = 100
export var FRICTION            = 800

onready var softCollision       = $SoftCollision
onready var stats               = $Stats
onready var WanderState         = $WanderState
onready var playerdetectionzone = $PlayerDetectionZone
onready var hurtbox             = $Hurtbox
onready var animationPlayer     = $AnimationPlayer
onready var sprite              = $Sprite
#ARRUMAR DEPOIS, USAR O VOADOR PRIMEIRO
var gravity
var idle
var no_move        = false
var can_move       = true
var state          = IDLE
var velocity       = Vector2.ZERO
var move_direction = 0

enum{IDLE, WANDER, CHASE}#, WALK}

func _ready():
	state=pick_random_state([IDLE,WANDER,]) #WALK])

func _physics_process(delta): 
	velocity.y+=20
	no_move=false
	if !GlobalSettings.inv_show:
		if can_move == true:
			match state:
				IDLE:
					$"Sprite".play("WALK") or $"Sprite".play("IDLE")
					velocity = velocity.move_toward(Vector2.ZERO,FRICTION*delta)
					seek_player()
					idle = move_and_collide(velocity*delta)
					if idle != null:
						velocity = Vector2(0,0)
						$"Sprite".play("IDLE")
					if WanderState.get_time_left()==0:
						update_wander()

				WANDER:
					seek_player()
					if WanderState.get_time_left()==0:
						update_wander()
					accelerate_towards_point(WanderState.target_position,delta)

					if  global_position.distance_to(WanderState.target_position)<= WANDER_TARGET_RANGE:
						update_wander()

				CHASE:
					$"Sprite".play("ATTACK")
					var player= playerdetectionzone.player
					if player != null:
						accelerate_towards_point(player.global_position,delta)
					else:
						state =IDLE
						$"Sprite".play("IDLE")
						
			if softCollision.is_colliding():
				velocity += softCollision.get_push_vector() * delta * 400
			velocity = move_and_slide(velocity)
		#velocity = move_and_slide(velocity)
			
func accelerate_towards_point(point,delta):
	var direction = global_position.direction_to(point)
	velocity = velocity.move_toward(direction*MAX_SPEED, ACCELERATION*delta)

func seek_player():
	#$GhostchaoSound.play()
	if playerdetectionzone.can_see_player():
		state = CHASE

func update_wander():
	state = pick_random_state([IDLE, WANDER])
	WanderState.start_wander_timer(rand_range(1, 5))
	
func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()

"""Qnd puder dar dano no bicho
func _on_Hurtbox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 200
	#hurtbox.create_hit_effect()
	hurtbox.start_invincibility(0.4)"""

func _on_Hitbox_area_entered(area):
	if area.name == "Hurtbox" :
		#é o knockback
		velocity= (area.global_position - self.global_position).normalized()*-200

func _on_Hurtbox_invincibility_started():
	animationPlayer.play("Start")

func _on_Hurtbox_invincibility_ended():
	animationPlayer.play("Stop")

func _on_Stats_no_health():
	$"Sprite".play("DEATH")
	yield($Sprite, "animation_finished")
	queue_free()
#ainda n testei , mas tem que terminar a animação de morte e dps sumir com o bichinho.
#var enemyDeathEffect = EnemyDeathEffect.instance() , pode dar algo de positivo ou negativo pro player

#ARRUMAR O FSM DO MONSTRINHO PRO IDLE E WALK E PARA ELE N FLUTUAR c:
