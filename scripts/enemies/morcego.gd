extends KinematicBody2D

export(int) var WANDER_TARGET_RANGE = 6
export(int) var ACCELERATION        = 450
export(int) var MAX_SPEED           = 150
export(int) var FRICTION            = 10

onready var softCollision:       Area2D          = $SoftCollision
onready var stats:               Node            = $Stats
onready var WanderState:         Node2D          = $WanderState
onready var playerdetectionzone: Area2D          = $PlayerDetectionZone
onready var hurtbox:             Area2D          = $Hurtbox
onready var animationPlayer:     AnimationPlayer = $AnimationPlayer
onready var sprite:              AnimatedSprite  = $Sprite

var direction        = 1
var can_move: bool   = true
var state            = IDLE
var velocity         = Vector2.ZERO
var move_direction   = 0

enum{IDLE, WANDER, CHASE}

func _ready():
	state=pick_random_state([IDLE,WANDER])

func _physics_process(delta): 
	if !GlobalSettings.inv_show:
		if !GlobalSettings.dialog_show:
			if can_move == true:
				match state:
					IDLE:
						$"Sprite".play("IDLE")
						velocity = velocity.move_toward(Vector2.ZERO,FRICTION*delta)
						flip()
						seek_player()
							
						if WanderState.get_time_left()==0:
							update_wander()

					WANDER:
						seek_player()
						if WanderState.get_time_left()==0:
							update_wander()
						accelerate_towards_point(WanderState.target_position,delta)
						flip()
						
						if  global_position.distance_to(WanderState.target_position)<= WANDER_TARGET_RANGE:
							update_wander()

					CHASE:
						#$"Sprite".play("ATTACK")
						$"Sprite".play("IDLE")
						var player= playerdetectionzone.player
						if player != null:
							accelerate_towards_point(player.global_position,delta)
							flip()
						else:
							state =IDLE
							$"Sprite".play("IDLE")
							
				if softCollision.is_colliding():
					velocity += softCollision.get_push_vector() * delta * 400
				velocity = move_and_slide(velocity)

func flip():
	if direction == 1:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true
	if look_there():
		direction = direction * -1
		$Sprite.flip_h = !$Sprite.flip_h

func look_there():
	if velocity.x < 0:
		direction = 1
	else:
		direction = -1

func accelerate_towards_point(point,delta):
	var direction = global_position.direction_to(point)
	velocity = velocity.move_toward(direction*MAX_SPEED, ACCELERATION*delta)

func seek_player():
	$GhostSound.play()
	if playerdetectionzone.can_see_player():
		state = CHASE

func update_wander():
	state = pick_random_state([IDLE, WANDER])
	WanderState.start_wander_timer(rand_range(1, 5))
	
func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()

"""Qnd puder dar dano no bicho
func _on_Hurtbox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 200
	#hurtbox.create_hit_effect()
	hurtbox.start_invincibility(0.4)"""

func _on_Hitbox_area_entered(area):
	if area.name == "Hurtbox" :
		#é o knockback
		velocity= (area.global_position - self.global_position).normalized()*-450

func _on_Hurtbox_invincibility_started():
	animationPlayer.play("Start")

func _on_Hurtbox_invincibility_ended():
	animationPlayer.play("Stop")

"""func _on_Stats_no_health():
	$"Sprite".play("IDLE")
	$"Sprite".play("DEATH")
	yield($Sprite, "animation_finished")
	TEM Q CONECTTAR D NOVO
	queue_free()
#ainda n testei , mas tem que terminar a animação de morte e dps sumir com o bichinho.
#var enemyDeathEffect = EnemyDeathEffect.instance() , pode dar algo de positivo ou negativo pro player"""


func _on_Hurtbox_area_entered(area):
	pass # Replace with function body.


func _on_Hurtbox_area_exited(area):
	pass # Replace with function body.
