extends Node2D

var e_enable  = false
var is_played = false
var is_phone_ringing = false
var answeres_phone = false#tem que salvar em um arquivo para não chamra de volta quando mudar de cena

func _ready():
	$Button.visible=false
#POR ALL PRA ATENDER A BAGAÇA
# Ficar parada ao atender o tel
func _input(event):
	if e_enable==true:
		if !is_played:
			if event.is_action_pressed("interact"):
				if !answeres_phone:
					answeres_phone = true
					$"telefone tocando".stop()
					var dialog = Dialogic.start('ConversationOnTelephone')
					add_child(dialog)
					GlobalSettings.dialog_show = true
					dialog.connect("dialogic_signal", self, "_dialogic_signal")
					dialog.connect('timeline_end', self, 'continue_with_game')

func continue_with_game(timeline):
	GlobalSettings.dialog_show = false

func _dialogic_signal(timeline_name):
	is_played = true
	$Button.visible=false

func _on_Area2D_body_entered(body):
	if !is_played:
		if !is_phone_ringing:
			is_phone_ringing = true
			$"telefone tocando".play()
		if !answeres_phone:
			e_enable=true
			$Button.visible=true

func _on_Area2D_body_exited(body):
	e_enable=false
	$Button.visible=false
