extends Area2D

export(PackedScene) var target_scene


var open     = false
var shape    = "CollisionShape2D"

func _ready():
	shape.set_deferred("disabled", true)

func _input(event):
	if event.is_action_pressed("ui_up"):
		if open == true:
			if !target_scene:
				print("NO WAY")
			return
		if get_overlapping_bodies().size()>0:
				next_scene()
			
func next_scene():
	var ERR=get_tree().change_scene_to(target_scene)
	
	if ERR!=OK:
		print("oops")
		
func open(body):
	if body.name == "Alisson" && body.keys > 0:
		body.keys -= 1
		open = true
