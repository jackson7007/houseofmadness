extends Popup

#Video Settings
onready var display_options = $Settingstabs/Video/MarginContainer/VideoSettings/OptionButton
onready var brightness_slider = $Settingstabs/Video/MarginContainer/VideoSettings/HBoxContainer/BrightnessSlider

#Audio Settings
onready var ambient_slider= $Settingstabs/Audio/MarginContainer/AudioSettings/AmbientSlider
onready var sound_effect_slider=$Settingstabs/Audio/MarginContainer/AudioSettings/SoundEffectSlider
onready var music_slider=$Settingstabs/Audio/MarginContainer/AudioSettings/MusicVolumeSlider
onready var voice_slider=$Settingstabs/Audio/MarginContainer/AudioSettings/VoiceVolumeSlider

func _ready():
	popup_centered()
	self.visible = false
	#get_tree().paused=!get_tree().paused

func map_slider_value_to_db(val):
	var inputA = 0 		# min slider value
	var inputB = 100 	# max slider value
	var outputA = -80 	# min db
	var outputB = 0 	# max db
	var result = (val - inputA) / (inputB - inputA) * (outputB - outputA) + outputA
	return result


func _on_OptionButton_item_selected(index):
	GlobalSettings.toggle_fullscreen(true if index ==1 else false)

func _on_BrightnessSlider_value_changed(value):
	GlobalSettings.update_brightness(value)
	
func _on_AmbientSlider_value_changed(value):
	GlobalSettings.update_master_vol(AudioServer.get_bus_index("Ambient"), map_slider_value_to_db(value))


func _on_SoundEffectSlider_value_changed(value):
	GlobalSettings.update_master_vol(AudioServer.get_bus_index("SoundEffect"), map_slider_value_to_db(value))


func _on_MusicVolumeSlider_value_changed(value):
	GlobalSettings.update_master_vol(AudioServer.get_bus_index("Music"), map_slider_value_to_db(value))


func _on_VoiceVolumeSlider_value_changed(value):
	GlobalSettings.update_master_vol(AudioServer.get_bus_index("Voice"), map_slider_value_to_db(value))


func _on_Back_pressed():
	self.visible = false
