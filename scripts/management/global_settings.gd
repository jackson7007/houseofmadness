extends Node

signal brightness_updated(value)

var inv_show    = false
var dialog_show = false

func toggle_fullscreen(value):
	OS.window_fullscreen= value
	
func update_brightness(value):
	emit_signal("brightness_updated",value)

func update_master_vol(number,vol):
	AudioServer.set_bus_volume_db(number,vol)
