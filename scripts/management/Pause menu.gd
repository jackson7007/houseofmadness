extends CanvasLayer

func _ready():
	pass

func _on_Button_pressed():
	get_tree().paused=false
	set_visible(false)
	self.queue_free()
	
func set_visible(is_visible):
	for node in get_children():
		node.visible=is_visible

func _on_Quit_pressed():
	get_tree().change_scene("res://scenes/title_screen/title_screen.tscn")


func _on_Options_pressed():
	$SettingsMenu.visible = true


func _on_SaveLoad_pressed():
	$Menu.visible=true
