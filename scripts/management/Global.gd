extends Node2D
#export(int) var max_health = 3 setget set_max_health
#var health           = max_health setget set_health
#var health_bar       = 0 setget set_health_bar

var from_level
var direction= Vector2.ZERO
var visited=[]
var Alisson = "Alisson"
var key_founds=[]
var opened_doors=["corredorkey"]
var dialog=[]
var monster_position=[]
var key_collect=[]
#var item_gather=[] por itens 1 por 1
var letter_collected=[]
var current_level
var data

# itens
#var letter=0
#var ...

#signal no_health
#signal health_changed(value)
#signal max_health_changed(value)
#signal health_bar_size(value)
#signal update_status

func _ready():
	#self.health=max_health
	pass # Replace with function body.
	
"""func set_max_health(value):
	max_health = value
	self.health = min(health, max_health)
	emit_signal("max_health_changed", max_health)

func set_health(value):
	health = value
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("no_health")

func set_health_bar(value):
	health_bar = value
	emit_signal("health_bar_size", health_bar)
	
func _on_reg_hp():
	if health < max_health:
		while health < max_health:
			if health_bar < 100:
				yield(get_tree().create_timer(1),"timeout")
				health_bar += 20
				emit_signal("health_bar_size", health_bar)
			else:
				health += 1
				health_bar = 0
				emit_signal("health_changed", health)
		if health == max_health:
			emit_signal("health_bar_size", health_bar)
			health_bar = 0
			
func _on_update_status():
	emit_signal("update_status")
"""

	
func save_game():
	data={

		"direction":[direction.x, direction.y],
		"from_level":from_level,
		"visited":visited,
		"key_founds":key_founds,
		"key_collect":key_collect,
		"opened_doors":opened_doors,
		"dialog":dialog,
		"Alisson": Alisson,
		"monster_position":monster_position,
	#	"item_gather":item_gather,
		"letter_collected":letter_collected,
		"current_level":current_level,
		#adicionar os outro intens aqui
	}
	
	var file =File.new()
	file.open("user://savegame.json",File.WRITE)
	var json=to_json(data)
	file.store_line(json)
	file.close()
	
func load_game():
	var file = File.new()
	if file.file_exists("user://savegame.json"):
		file.open("user://savegame.json", File.READ)
		data = parse_json(file.get_as_text())
		file.close()

		current_level = data.current_level
		opened_doors  = data.opened_doors
		key_founds= data.key_founds
		key_collect=data.key_collect
		visited = data.visited
		Alisson = data.Alisson
		direction= Vector2(data.direction[0], data.direction[1])
#		health= data.health
		#max_health= data.max_health
		visited=visited
		dialog=dialog
		monster_position=monster_position
		#item_gather=item_gather
		#listar os itens tb
		letter_collected=letter_collected

		

		
		
		



