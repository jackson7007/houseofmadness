extends Node

#ARRUMAR, Por músics
func _ready():
	#Audiostream.play_game_over_music()
	$"CanvasLayer/AnimationPlayer".play("GameOver")

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://scenes/global/TryAgain.tscn")
