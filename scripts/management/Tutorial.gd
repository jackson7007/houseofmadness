extends Node

export (Resource) var obj_game

func _ready():
	if !obj_game.Tutorial:
		obj_game.Tutorial=true
		ResourceSaver.save("res://Pressets/new_resource.res",obj_game)
		get_tree().change_scene("res://scenes/global/MiniTutorial1.tscn")
	pass
