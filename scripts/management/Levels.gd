extends Node2D

#https://www.youtube.com/watch?v=qaznUllK7Hg&list=UURnl39p4zUm_fryejPrxyEQ&index=14
#Olahr aqui https://www.youtube.com/watch?v=tN76BJ2XyDQ
onready var paused_scene = preload("res://scenes/pause_menu.tscn")

func _ready():
	Audiostream.play_sala_1_music()
	Global.current_level = self.name
	if Global.from_level!=null:
		get_node("Node/Alisson").set_position(get_node(Global.from_level + "Pos").position)
		
func _input(event):
	if event.is_action_pressed("pause"):
		var obj = paused_scene.instance()
		add_child(obj)
		get_tree().paused=!get_tree().paused
