extends MarginContainer

export(PackedScene) var slot_scene

var slot_list = Array()
var inv_comp: Inventory_Component

func _ready():
	for i in range(inv_comp.inv_slots):
		var slot_child = slot_scene.instance()
		slot_child.slot_index = i
		$Background/InventoryGrid.add_child(slot_child)
		slot_list.append(slot_child)

func _on_CloseButton_pressed():
	GlobalSettings.inv_show = false
	queue_free()

"""func pegaImagem(minhaImagem):
	$Background/TextureRect.texture    = minhaImagem
	$Background/TextureRect.rect_scale = Vector2(1.0, 1.0)
	$Background/TextureRect.rect_scale = $Background/TextureRect.rect_scale * 0.060
"""
