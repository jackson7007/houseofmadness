extends Control

signal interacted(sender, item, amount)

export(Script) var directory
export(int) var amount = 1
onready var player_pickup = false

var item
var can=false

func _ready():
	var player = get_tree().get_nodes_in_group("Player")[0]
	var _succ = connect("interacted", player, "_on_item_interacted")
	
	if is_instance_valid(directory):
		item = directory.new()
		$Button/TextureRect.texture = item.i_image

func _input(event):
	if can==true:
		if event.is_action_pressed("interact"):
			if is_instance_valid(directory):
				emit_signal("interacted", self, item, amount)

func _on_pressed():
	can =true
	if is_instance_valid(directory):
		emit_signal("interacted", self, item, amount)

func _on_Area2D_body_entered(body):
	can = true

func _on_Area2D_body_exited(body):
	can = false
