extends Node2D

signal door_opened

var keytaken     = false
var in_door_zone = false

"""func _init():
	self.i_name        = "Chave"
	self.i_image       = load("res://assets/inventory/itens_icons/key.png")
	self.i_description = "Maybe I can use this in somewhere..."
	self.i_consumable  = false
	self.i_stackable   = false"""

func i_have(Alisson):
	if keytaken ==false:
		keytaken =true
		$key.queue_free()

func _process(delta):
	if keytaken == true:
		if in_door_zone == true:
			if Input.is_action_pressed("interact"):
				print("ABRINDO")
				emit_signal("door_opened")
			

func _on_Porta_body_entered(Alisson):
	in_door_zone = true
	print("ondoor")

func _on_Porta_body_exited(Alisson):
	in_door_zone = false
	print("Who can it be now ?")
