extends Area2D

var item_name

var player          = null
var being_gather_up = false

func _ready():
	pass

func gathered_item(body):
	$"../Popup".popup()
	player = body
	being_gather_up = true

func _on_GatherLetter_body_entered(body):
	gathered_item(body)
