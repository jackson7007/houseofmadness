extends Node2D

var e_enable=false

func _input(event):
	if e_enable==true:
		if event.is_action_pressed("interact"):
				$Bilhete/fundo.visible=!$Bilhete/fundo.visible
				$Bilhete/Label.visible=!$Bilhete/Label.visible
				$Bilhete/bilhete.visible=!$Bilhete/bilhete.visible

func _on_Area2D_body_entered(body):
	$Button.visible=true
	e_enable=true


func _on_Area2D_body_exited(body):
	$Button.visible=false
	e_enable=false
