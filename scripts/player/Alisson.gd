extends KinematicBody2D

onready var hurtbox    = $Hurtbox
onready var anime      = $AnimationAll
onready var sprite     = $Sprite
onready var gui        = get_node(gui_path)

export(NodePath) var gui_path = "GUI"

const ACCELERATION = 100
const MAX_SPEED    = 100
const FRICTION     = 2000

var damage
var current_soul
var soul: int                  = 100          
var velocity                   = Vector2.ZERO
var direction                  = Vector2.DOWN
var can_move: bool             = true 
var can_attack: bool           = true 
var stars_direction            = Vector2.ZERO
var stars_direction_multiplier = -0.5
var state                      = MOVE
var hide: bool                 = false
var walk_light: bool           = false

enum { MOVE,}# ATTACK,}

func _ready():
	current_soul = soul

func _input(event):
	if event.is_action_pressed("inventory"):
		$InventoryComponent.toggle_window(self)

func _physics_process(delta):
	if GlobalSettings.dialog_show:
		$AnimationAll.play("Tel")
	if !GlobalSettings.inv_show:
		if !GlobalSettings.dialog_show:
			if can_move == true:
				match state:
					MOVE:
						move_state(delta)
					#ATTACK:
						#attack_state()

func move_state(delta):
	Global.direction
	var input_vector = Vector2.ZERO
	var input_y = Input.is_action_pressed("ui_down") 
	input_vector.x   = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y   = Input.get_action_strength("ui_down") 
	input_vector     = input_vector.normalized()
	velocity     = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)

	var input_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if input_x < 0:
		sprite.flip_h = true
	elif input_x > 0:
		sprite.flip_h = false
		
	if input_x != 0:
		if sprite.flip_h == true:
			$Camera2D/Dust.process_material.gravity = Vector3(100, 0, 0)
		else:
			$Camera2D/Dust.process_material.gravity = Vector3(-100, 0, 0)

		velocity = velocity.move_toward(Vector2(input_x, 0) * MAX_SPEED, ACCELERATION * delta)
		
		if !walk_light:
			anime.play("Walk", -1, 2)
		else:
			anime.play("Walk_light", -1, 2)
	else:
		$Camera2D/Dust.process_material.gravity = Vector3(0,0,0)
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		
		if !walk_light:
			anime.play("Idle")
		else:
			anime.play("Idle_light")

		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)

	if Input.is_action_pressed("ui_down"):
		anime.play("Crouch")
		if input_x < 0:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
			sprite.flip_h = true
		elif input_x > 0:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
			sprite.flip_h = false
		
	#if can_attack == true:
		#if Input.is_action_just_pressed("ui_attack"):
		#	state = ATTACK
			#_power()
	velocity = move_and_slide(velocity, Vector2.UP)
	move()

func move():
	velocity = move_and_slide(velocity)
	
#func attack_state():
	#velocity = Vector2.ZERO
	#animationState.travel(Global.player + "Attack")

#func attack_animation_finished():
	#state = MOVE

func _on_Hurtbox_area_entered(area):
	if area.name == "PlayerDetectionZone":
		self.OnHit(15)
	hurtbox.start_invincibility(0.6)
#	hurtbox.create_hit_effect()
	#create_hurt_effect()
	#var HurtSound = PlayerHurtSound.instance()
	if soul == 0:
		 get_tree().change_scene("res://scenes/global/Game Over.tscn")

"""func _on_Hurtbox_invincibility_started():
	#POr animação de se recuperar ? 

func _on_Hurtbox_invincibility_ended():
	#Por animação de ter se recuperado ?"""
	
func OnHit(damage):
	current_soul -= damage
	if current_soul <= 0:
		get_tree().change_scene("res://scenes/global/Game Over.tscn")
	
func _on_item_interacted(sender, item, amount):
	if $InventoryComponent.add_to_inventory(item, amount):
		sender.queue_free()

func safe():

	pass
