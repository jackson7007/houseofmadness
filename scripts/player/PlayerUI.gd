extends CanvasLayer

onready var soul_globe       = get_node("SoulUI/TextureProgress")
onready var soul_globe_tween = get_node("SoulUI/TextureProgress/Tween")

func _ready():
	soul_globe.max_value = get_node("../Alisson").soul
	soul_globe.value     = get_node("../Alisson").soul


func _physics_process(delta):
	UptadeGlobe()

func UptadeGlobe():
	var new_soul = get_node("../Alisson").current_soul
	soul_globe_tween.interpolate_property(soul_globe, 'value', soul_globe.value, new_soul, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	soul_globe_tween.start()
	
	if new_soul > 85:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/1.png")
	
	if new_soul == 85:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/2.png")
	
	if new_soul == 70:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/3.png")
	
	if new_soul == 55:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/4.png")
	
	if new_soul == 40:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/5.png")
	
	if new_soul == 25:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/6.png")
	
	if new_soul == 10:
		$"SoulUI/TextureProgress/IconStatus".texture = load("res://assets/player/StatusPictures/7.png")
		

